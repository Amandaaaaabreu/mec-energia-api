

import json
import pytest

from datetime import date, datetime
from unittest.mock import patch, Mock 


from rest_framework import status
from rest_framework.test import APIClient

from tests.test_utils import dicts_test_utils
from tests.test_utils import create_objects_test_utils

from universities.models import ConsumerUnit


@pytest.mark.django_db
class TestConsumerUnitsGetEnergyBills:
    def setup_method(self):
        self.year = {
            'valid_year_with_no_bills': 2015,
            'valid_year_with_bills': 2022,
            'year_greater_than_today': 2027,
            'year_lesser_than_today': 2000,
        }

        self.university_dict = dicts_test_utils.university_dict_1
        self.user_dict = dicts_test_utils.university_user_dict_1

        self.university = create_objects_test_utils.create_test_university(self.university_dict)
        self.user = create_objects_test_utils.create_test_university_user(self.user_dict, self.university)

        self.consumer_unit_to_be_created = {
            'name': 'Faculdade do Gama',
            'code': '111111111',
            'created_on': '2022-10-02',
            'is_active': True,
            'university': self.university
        }

        
        self.energy_bill_dictionary = {
            "id": 200,
            "date": datetime.now(),  # Substitua por um valor de data real, se necessário
            "invoice_in_reais": 150.75,
            "is_atypical": False,
            "peak_consumption_in_kwh": 300,
            "off_peak_consumption_in_kwh": 200,
            "peak_measured_demand_in_kw": 50,
            "off_peak_measured_demand_in_kw": 30,
        }
        
    @patch('universities.models.ConsumerUnit.date', date(year=2014, month=1, day=1))  #CT1
    def test_fetch_energy_bills_for_year_should_not_return_any_bill(self):

        consumer_unit = ConsumerUnit(**self.consumer_unit_to_be_created)
        consumer_unit.save()
        consumer_unit.refresh_from_db()

        energy_bills = consumer_unit.get_energy_bills_by_year(self.year['valid_year_with_no_bills'])
        for bill in energy_bills:
            assert bill['energy_bill'] == None

    @patch('universities.models.ConsumerUnit.date', date(year=2014, month=1, day=1))   #CT2
    def test_get_energy_bills_by_year_should_raise_exception_for_greater_year(self):
        consumer_unit = ConsumerUnit(**self.consumer_unit_to_be_created)
        consumer_unit.save()
        consumer_unit.refresh_from_db()

        with pytest.raises(Exception) as excinfo:
            consumer_unit.get_energy_bills_by_year(self.year['year_greater_than_today'])

        assert str(excinfo.value) == "Consumer User do not have Energy Bills this year"

    @patch('universities.models.ConsumerUnit.date', date(year=2014, month=1, day=1))   #CT3
    def test_fetch_energy_bills_for_year_should_raise_exception_for_lesser_year(self):

        consumer_unit = ConsumerUnit(**self.consumer_unit_to_be_created)
        consumer_unit.save()
        consumer_unit.refresh_from_db()

        with pytest.raises(Exception) as excinfo:
            consumer_unit.get_energy_bills_by_year(self.year['year_lesser_than_today'])

        assert str(excinfo.value) == "Consumer User do not have Energy Bills this year"

    @patch('universities.models.ConsumerUnit.date', date(year=2014, month=1, day=1))    #CT4
    @patch('universities.models.EnergyBill.get_energy_bill')
    @patch('universities.models.EnergyBillUtils.energy_bill_dictionary')
    def test_fetch_energy_bills_for_year_should_return_some_bills(
        self, mock_energy_bill_dict, mock_get_energy_bill
    ):
        consumer_unit = ConsumerUnit(**self.consumer_unit_to_be_created)
        consumer_unit.save()
        consumer_unit.refresh_from_db()

        mock_get_energy_bill.return_value = Mock()
        mock_energy_bill_dict.return_value = self.energy_bill_dictionary

        energy_bills = consumer_unit.get_energy_bills_by_year(self.year['valid_year_with_bills'])

        assert len(energy_bills) == 12
        for bill in energy_bills:
            assert bill['energy_bill'] == self.energy_bill_dictionary

  